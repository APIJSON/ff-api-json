# FfApiJson

#### 介绍
  只支持查询  
 用JSON格式直接生成sql 
 借鉴APIJSON
  支持多数据源

#### 软件架构
 SpringBoot
 mybatisPlus
 fastJson


#### 安装教程

1.  把sql导入到自己的数据库中
2.	sql表中有测试表和配置表 
		其中mydatasource这个表用来配置多数据源
		table_mapper这个用于表的映射,可以防止直接暴露表名 
		其他表均为测试数据表
3.  项目导入到编程软件当中
	把配置文件中的数据源配置修改成自己本地的
4.  运行项目

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 下一步
1. 表名映射(初步完成)
2. 接入缓存（缓存做成可插拔式的） 
      缓存的key  YYYYMMdd_Hash(param)
3. 异常统一处理
4. 处理Like子句 (简单处理  like '%value%‘)
5. 执行sql,返回结果
6. PostMan 压测

#### 前端使用
1. 获取后端开发人员提前写好的对象
    ![Image text](https://gitee.com/own_3_0/ff-api-json/raw/master/img/code-snapshot%20(1).png)
2. 根据条件
  ![Image text](https://gitee.com/own_3_0/ff-api-json/raw/master/img/code-snapshot%20(4).png)
3.调用请求
  ![Image text](https://gitee.com/own_3_0/ff-api-json/raw/master/img/code-snapshot%20(2).png)
4.获取结果
  ![Image text](https://gitee.com/own_3_0/ff-api-json/raw/master/img/code-snapshot%20(3).png)