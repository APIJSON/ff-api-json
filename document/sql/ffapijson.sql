/*
Navicat MySQL Data Transfer

Source Server         : xg
Source Server Version : 50710
Source Host           : localhost:3306
Source Database       : ffapijson

Target Server Type    : MYSQL
Target Server Version : 50710
File Encoding         : 65001

Date: 2021-03-08 17:38:31
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for clazz
-- ----------------------------
DROP TABLE IF EXISTS `clazz`;
CREATE TABLE `clazz` (
  `classid` int(11) NOT NULL AUTO_INCREMENT,
  `cname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of clazz
-- ----------------------------
INSERT INTO `clazz` VALUES ('1', '.net1');
INSERT INTO `clazz` VALUES ('2', '网开3');
INSERT INTO `clazz` VALUES ('3', '网开4');
INSERT INTO `clazz` VALUES ('4', '网开5');

-- ----------------------------
-- Table structure for mydatasource
-- ----------------------------
DROP TABLE IF EXISTS `mydatasource`;
CREATE TABLE `mydatasource` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `ip` varchar(255) NOT NULL COMMENT 'ip 127.0.0.1',
  `port` varchar(255) NOT NULL COMMENT '端口号 3306',
  `ff_schema` varchar(255) DEFAULT NULL COMMENT '数据库名',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `driver_name` varchar(255) DEFAULT NULL COMMENT '驱动',
  `key_name` varchar(255) DEFAULT NULL COMMENT '唯一标识',
  `is_valid` tinyint(4) DEFAULT '1' COMMENT '是否有效，默认有效',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `creator` varchar(255) DEFAULT NULL COMMENT '创建人',
  `updator` varchar(255) DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mydatasource
-- ----------------------------
INSERT INTO `mydatasource` VALUES ('4', '127.0.0.1', '3306', 'ffapijson', 'root', '123456', 'com.mysql.jdbc.Driver', 'bdsj', '1', '2021-03-08 16:15:31', '2021-03-08 16:15:35', 'x', 'x');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `sid` int(11) NOT NULL AUTO_INCREMENT,
  `sname` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `sage` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  PRIMARY KEY (`sid`),
  KEY `yj` (`classid`),
  CONSTRAINT `yj` FOREIGN KEY (`classid`) REFERENCES `clazz` (`classid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('1', 'jim', '男', '18', '1');
INSERT INTO `student` VALUES ('2', 'tom', '男', '27', '2');
INSERT INTO `student` VALUES ('3', 'mary', '女', '20', '3');
INSERT INTO `student` VALUES ('4', 'rose', '女', '22', '2');
INSERT INTO `student` VALUES ('5', 'jackson', '男', '25', '2');
INSERT INTO `student` VALUES ('6', 'jacky', '女', '21', '3');
INSERT INTO `student` VALUES ('7', 'test', '男', '22', '1');
INSERT INTO `student` VALUES ('8', 'f', '男', '12', '4');

-- ----------------------------
-- Table structure for table_mapper
-- ----------------------------
DROP TABLE IF EXISTS `table_mapper`;
CREATE TABLE `table_mapper` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `origin_table_name` varchar(255) DEFAULT NULL COMMENT '原表名',
  `expose_table_name` varchar(255) DEFAULT NULL COMMENT '暴露表名',
  `is_valid` tinyint(4) DEFAULT '1' COMMENT '是否有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of table_mapper
-- ----------------------------
INSERT INTO `table_mapper` VALUES ('1', 'maincase', 'maincase_mapper', '1');
