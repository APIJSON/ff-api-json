1.  目前支持的操作符为

   

|        |                   | 使用示例            |
| ------ | ----------------- | ------------------- |
| >      | @gt               | "classid@gt":3      |
| >=     | @ge               | "classid@ge":3      |
| =      | @eq               | "classid@eq":3      |
| !=     | @!eq              | "classid@!eq":3     |
| <      | @lt               | "classid@lt":3      |
| <=     | @le               | "classid@le":3      |
| in     | @in               | "classid@in":3,4    |
| not in | @notIn    (i大写) | "classid@notIn":3,4 |



如果是**数字类型**的  在后面加个**Int**即可，如  **@gt**变为 **@gtInt**