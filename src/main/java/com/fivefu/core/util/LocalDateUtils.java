package com.fivefu.core.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/9
 */
public class LocalDateUtils {

    public final static String NO_LINK_DAY_FORMAT = "YYYYMMdd";

    public static String getTodayNoLink() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(NO_LINK_DAY_FORMAT);
        return LocalDate.now().format(dateTimeFormatter);
    }

}
