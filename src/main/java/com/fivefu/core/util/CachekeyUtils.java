package com.fivefu.core.util;


import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.pojo.FfSql;
import com.fivefu.core.param.FFParam;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/9
 */
public class CachekeyUtils {


    public static CacheItem getKeyByJson(JSONObject jsonObject, FfSql ffSql) {

        if (null == jsonObject) {
            return null;
        }
        String ds = jsonObject.containsKey(FFParam.KEY_DATASOURCE) ? StrUtil
                .emptyToDefault(jsonObject.getString(FFParam.KEY_DATASOURCE), "master") : "master";
        //key  有三部分组成 第一部分 key  第二部分  数据源  第三部分 sql的摘要
        String key = LocalDateUtils.getTodayNoLink() + "_" + ds + myHash(ffSql);
        CacheItem cacheItem = new CacheItem(key, 2);
        //默认是2分钟缓存
        Integer cacheTime = jsonObject.getInteger(FFParam.KEY_CACHE);
        if (null == cacheTime) {
            return cacheItem;
        }
        cacheItem.setMinutes(cacheTime > 120 ? 120 : cacheTime < 0 ? 2 : cacheTime);
        return cacheItem;


    }

    public static String myHash(FfSql ffSql) {
        String md5Hex1 = DigestUtil.md5Hex(ffSql.createCommonSql());
        return md5Hex1;
    }


    public static class CacheItem {

        String key;
        int minutes = 2;

        public CacheItem(String key, int minutes) {
            this.key = key;
            this.minutes = minutes;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }
    }

}
