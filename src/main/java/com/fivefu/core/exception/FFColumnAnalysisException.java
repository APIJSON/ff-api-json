package com.fivefu.core.exception;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/5
 */
public class FFColumnAnalysisException extends FfSqlAnalysisException {

    public FFColumnAnalysisException() {
    }

    public FFColumnAnalysisException(String msg) {
        super(msg);
    }
}
