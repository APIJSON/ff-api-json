package com.fivefu.core.exception;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/5
 */
public class FfSqlAnalysisException extends  Exception {

    public FfSqlAnalysisException() {
    }

    public FfSqlAnalysisException(String message) {
        super(message);
    }
}
