package com.fivefu.core.exception;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/5
 */
public class FFNoSuchTableException extends  FfSqlAnalysisException {

    public FFNoSuchTableException() {
    }

    public FFNoSuchTableException(String message) {
        super(message);
    }
}
