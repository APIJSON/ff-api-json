package com.fivefu.core.exception;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
public class FfParamException extends  Exception {

    public FfParamException()
    {
        super();
    }

    public FfParamException(String msg){
          super(msg);
    }

}
