package com.fivefu.core.exception;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/5
 */
public class FfNoSupportException extends  FfSqlAnalysisException {

    public FfNoSupportException() {
    }

    public FfNoSupportException(String message) {
        super(message);
    }
}
