package com.fivefu.core.config;

import com.fivefu.core.pojo.ResultInfo;
import java.io.IOException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Author zhf
 * @Description 异常拦截类
 * @Date 2021/3/10
 */
@RestControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(value = Exception.class)
    public ResultInfo handler(Exception e) {
        e.printStackTrace();
        if (e instanceof IOException) {
            return new ResultInfo(501, "请求体要是JSON格式" + e.getMessage());
        }
        return ResultInfo.renderError(e.getMessage());
    }
}
