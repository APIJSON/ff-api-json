package com.fivefu.core.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/8
 */
public interface FfExecuteSqlMapper {

    public List<Map<String,Object>> executeCommonSql(@Param("sql") String sql);


    public Integer executeCountSql(@Param("countSql") String sql);
}
