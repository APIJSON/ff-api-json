package com.fivefu.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fivefu.core.pojo.FfDataSource;
import java.util.List;
import java.util.Map;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */


public interface FfDataSourceMapper extends BaseMapper<FfDataSource> {


    List<Map<String, Object>> getTableInfo();
}
