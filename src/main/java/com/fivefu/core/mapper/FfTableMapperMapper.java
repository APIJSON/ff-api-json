package com.fivefu.core.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fivefu.core.pojo.FFTableMapper;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/8
 */
public interface FfTableMapperMapper extends BaseMapper<FFTableMapper> {


}
