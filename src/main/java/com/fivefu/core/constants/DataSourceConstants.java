package com.fivefu.core.constants;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
public class DataSourceConstants {

    /**
     * master数据源
     */
    public static final String DS_KEY_MASTER = "master";
    /**
     * slave数据源
     */
    public static final String DS_KEY_SLAVE = "slave";

}
