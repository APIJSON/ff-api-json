package com.fivefu.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fivefu.core.pojo.FfDataSource;
import java.util.List;
import java.util.Map;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
public interface FfDataSourceService extends IService<FfDataSource> {
         List<Map<String,Object>>  getTableInfo();

}
