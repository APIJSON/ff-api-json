package com.fivefu.core.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fivefu.core.pojo.FfDataSource;
import com.fivefu.core.mapper.FfDataSourceMapper;
import com.fivefu.core.service.FfDataSourceService;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
@Service
public class FfDataSourceImpl extends ServiceImpl<FfDataSourceMapper, FfDataSource> implements FfDataSourceService {

    @Override
    public List<Map<String, Object>> getTableInfo() {
        return baseMapper.getTableInfo();
    }
}
