package com.fivefu.core.service;

import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.pojo.FfSql;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/9
 */

public interface FfCacheService {
     Object getResultByKey(JSONObject json, FfSql ffSql);


}
