package com.fivefu.core.handler;

import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.exception.FfSqlAnalysisException;
import com.fivefu.core.param.FFParam;
import com.fivefu.core.param.FFTableMapperParam;
import com.fivefu.core.pojo.FfSql;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/12
 */
@Component
public class WhereSqlHandler implements  SqlHandler {

    @Override
    public void dealSql(JSONObject jsonObject, FfSql ffSql) throws FfSqlAnalysisException {
        List<String> list = ffSql.getUnProcessedTableNameList();
        StringBuffer whereBuffer = new StringBuffer();
        for (String s : list) {
            JSONObject tableItem =     jsonObject.getJSONObject(FFTableMapperParam.getExposeTableName(s));

            if (tableItem.containsKey(FFParam.KEY_WHERE)) {
                //where条件
                JSONObject whereObject = tableItem.getJSONObject(FFParam.KEY_WHERE);
                for (Map.Entry entry : whereObject.entrySet()) {
                    String key = entry.getKey().toString(); // column@operator
                    if (key.contains("@")) {
                        String arr[] = key.split("@");
                        if (arr.length == 2) {
                            String column = arr[0];
                            String operator = arr[1];
                            if (whereBuffer.length() > 0) {
                                whereBuffer.append(" and");
                            }
                            whereBuffer
                                    .append(FFParam.dealWhereParam(s, column, operator, entry.getValue().toString()));

                        }


                    }
                }
                tableItem.remove(FFParam.KEY_WHERE);
            }

        }
        if (whereBuffer.length() > 0) {
            ffSql.setWhere(" where " + whereBuffer.toString());
        }
    }

    @Override
    public int order() {
        return 2;
    }

    private String errorMsg;

    @Override
    public String getErrMsg() {
        return this.errorMsg;
    }

    protected void setErrorMsg(String msg){
        this.errorMsg = msg;
    }
}
