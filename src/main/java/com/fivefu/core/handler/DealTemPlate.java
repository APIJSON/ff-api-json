package com.fivefu.core.handler;

import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.param.FFTableMapperParam;
import com.fivefu.core.pojo.FfSql;
import java.util.List;

/**
 * @Author zhf
 * @Description 模板  处理模板
 * @Date 2021/3/8
 */
public class DealTemPlate {

    public static String dealTemplate(JSONObject jsonObject, FfSql ffSql, String key) {
        List<String> list = ffSql.getUnProcessedTableNameList();
        StringBuffer templateBuffer = new StringBuffer();
        for (String s : list) {
            JSONObject tableObject =    jsonObject.getJSONObject(FFTableMapperParam.getExposeTableName(s));
            if (tableObject.containsKey(key)) {
                String orderStr = tableObject.getString(key);
                String orderArr[] = orderStr.split(",");
                for (String item : orderArr) {
                    if (templateBuffer.length() > 0) {
                        templateBuffer.append(", ");
                    }
                    templateBuffer.append(item);
                }
                tableObject.remove(key);
            }
        }
        return templateBuffer.toString();
    }

    public static String dealTemplate(JSONObject jsonObject, FfSql ffSql, String key,String ext) {
        List<String> list = ffSql.getUnProcessedTableNameList();
        StringBuffer templateBuffer = new StringBuffer();
        for (String s : list) {
            JSONObject tableObject =    jsonObject.getJSONObject(FFTableMapperParam.getExposeTableName(s));
            if (tableObject.containsKey(key)) {
                String orderStr = tableObject.getString(key);
                String orderArr[] = orderStr.split(",");
                for (String item : orderArr) {
                    if (templateBuffer.length() > 0) {
                        templateBuffer.append(", ");
                    }
                    templateBuffer.append(item).append(" ").append(ext);
                }
                tableObject.remove(key);
            }
        }
        return templateBuffer.toString();
    }
}
