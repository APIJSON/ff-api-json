package com.fivefu.core.handler;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.exception.FfSqlAnalysisException;
import com.fivefu.core.param.FFParam;
import com.fivefu.core.pojo.FfSql;
import java.util.StringJoiner;
import org.springframework.stereotype.Component;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/12
 */
@Component
public class OrderHandler implements SqlHandler {

    @Override
    public void dealSql(JSONObject jsonObject, FfSql ffSql) throws FfSqlAnalysisException {
        String order = DealTemPlate.dealTemplate(jsonObject, ffSql, FFParam.KEY_ORDER);
        String orderDesc = DealTemPlate.dealTemplate(jsonObject, ffSql, FFParam.KEY_ORDER_DESC, "desc");
        String orderAsc = DealTemPlate.dealTemplate(jsonObject, ffSql, FFParam.KEY_ORDER_ASC);
        StringJoiner stringJoiner = new StringJoiner(",");
        if (StrUtil.isNotBlank(order)) {
            stringJoiner.add(order);
        }

        if (StrUtil.isNotBlank(orderDesc)) {
            stringJoiner.add(orderDesc);
        }
        if (StrUtil.isNotBlank(orderAsc)) {
            stringJoiner.add(orderAsc);
        }

        if (stringJoiner.length() > 0) {
            ffSql.setOrder(" order by " + stringJoiner.toString());
        }


    }

    @Override
    public int order() {
        return 3;
    }

    private String errorMsg;

    @Override
    public String getErrMsg() {
        return this.errorMsg;
    }

    protected void setErrorMsg(String msg) {
        this.errorMsg = msg;
    }
}
