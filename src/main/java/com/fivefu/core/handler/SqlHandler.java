package com.fivefu.core.handler;

import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.exception.FfSqlAnalysisException;
import com.fivefu.core.pojo.FfSql;

public interface SqlHandler {

    void dealSql(JSONObject jsonObject, FfSql ffSql) throws FfSqlAnalysisException;

    // 0 tablename datasource
    // 1 column  where
    // 2 on
    // 3group order limit
    // 4 page
    int order();

    String getErrMsg();
}
