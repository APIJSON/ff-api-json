package com.fivefu.core.handler;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.exception.FfSqlAnalysisException;
import com.fivefu.core.param.FFParam;
import com.fivefu.core.pojo.FfSql;
import org.springframework.stereotype.Component;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/12
 */
@Component
public class DataSourceHandler implements  SqlHandler {



    @Override
    public void dealSql(JSONObject jsonObject, FfSql ffSql) throws FfSqlAnalysisException {
        String ds =  jsonObject.getString(FFParam.KEY_DATASOURCE);
        if(!StrUtil.isEmpty(ds)){
            ffSql.setDs(ds);
        }
    }

    @Override
    public int order() {
        return 0;
    }

    private String errorMsg;

    @Override
    public String getErrMsg() {
        return this.errorMsg;
    }

    protected void setErrorMsg(String msg){
        this.errorMsg = msg;
    }
}
