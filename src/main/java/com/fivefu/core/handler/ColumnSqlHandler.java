package com.fivefu.core.handler;

import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.exception.FFColumnAnalysisException;
import com.fivefu.core.param.FFParam;
import com.fivefu.core.param.FFTableMapperParam;
import com.fivefu.core.pojo.FfSql;
import java.util.HashSet;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/12
 */
@Component
public class ColumnSqlHandler implements SqlHandler {

    private String error;

    @Override
    public void dealSql(JSONObject jsonObject, FfSql ffSql) throws FFColumnAnalysisException {
        List<String> list = ffSql.getUnProcessedTableNameList();
        StringBuffer columnBuffer = new StringBuffer();
        HashSet<String> columnSet = new HashSet<>();
        for (String s : list) {
            JSONObject item = jsonObject.getJSONObject(FFTableMapperParam.getExposeTableName(s));

            if (item.containsKey(FFParam.KEY_COLUMN)) {
                String clolumnStr = item.getString(FFParam.KEY_COLUMN);
                String[] columnArr = clolumnStr.split(",");
                for (String columnItem : columnArr) {
                    if (columnItem.indexOf(";") != -1) {
                        throw new FFColumnAnalysisException(s + FFParam.KEY_COLUMN + columnItem + "不能以;分分隔，多列请用,分隔");
                    }
                    if (columnItem.contains(":")) { //a:b
                        String alis[] = columnItem.split(":");
                        if (alis.length == 2) {
                            if (columnBuffer.length() > 0) {
                                columnBuffer.append(", ");
                            }
                            if (alis[0].contains("(") && alis[0].contains(")")) {
                                columnBuffer
                                        .append(alis[0]).append(" ").append(alis[1]);//不要表前缀
                            } else {
                                if (columnSet.contains(alis[0])) {
                                    columnBuffer
                                            .append(" ").append(s).append(".")//增加表前缀
                                            .append(alis[0]).append(" ").append(alis[1]);
                                } else {
                                    columnBuffer.append(alis[0]).append(" ").append(alis[1]);
                                }
                                columnSet.add(alis[0]);
                            }

                        } else {
                            throw new FFColumnAnalysisException(
                                    s + FFParam.KEY_COLUMN + columnItem + "别名应以 a:b 的形式，多列 a:b,a1:b1....");
                        }
                    } else { // a,b
                        if (columnBuffer.length() > 0) {
                            columnBuffer.append(", ");
                        }
                        columnBuffer.append(s).append(".").append(columnItem);
                    }
                }

            } else {
                if (list.size() == 1) {
                    columnBuffer.append(" ").append(s).append(".*");
                }

            }
            item.remove(FFParam.KEY_COLUMN);
        }

        ffSql.setColumnName(columnBuffer.toString());
    }

    @Override
    public int order() {
        return 1;
    }

    @Override
    public String getErrMsg() {
        return this.error;
    }

    protected String setErrMsg(String msg) {
        this.error = msg;
        return getErrMsg();
    }
}
