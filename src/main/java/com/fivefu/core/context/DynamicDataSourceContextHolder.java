package com.fivefu.core.context;

import cn.hutool.core.util.StrUtil;
import com.fivefu.core.constants.DataSourceConstants;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
public class DynamicDataSourceContextHolder {

    public static final  ThreadLocal<String>  ds_key = new ThreadLocal<>();

    /*
     设置数据源
     */
    public  static  void setContextKey(String key){
        System.out.println("切换到的数据源为:"+key);
        ds_key.set(key);
    }
    /*
      获取数据源名称
     */
    public  static String getContextKey(){
        String key = ds_key.get();
        return StrUtil.isEmptyIfStr(key) ? DataSourceConstants.DS_KEY_MASTER : key;
    }

    public static  void  removeKey(){
        ds_key.remove();
    }


}
