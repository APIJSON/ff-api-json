package com.fivefu.core.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
@Component
public class FfSpringContextHolder implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        FfSpringContextHolder.applicationContext = applicationContext;
    }

    public static ApplicationContext getContext() {
        return FfSpringContextHolder.applicationContext;
    }
}
