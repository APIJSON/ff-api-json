package com.fivefu.core.param;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/8
 */
//表 映射
    /*
    *    这个类的作用是建立原表和映射表的映射关系 以及发粘性映射关系
     */
public class FFTableMapperParam {

    private static Map<String, String> tableMapperMap = new HashMap<>();
    private static Map<String, String> tableMapperReverseMap = new HashMap<>();

    public static void setTableMapperMap(Map<String, String> tableMapperMap) {
        FFTableMapperParam.tableMapperMap = tableMapperMap;
    }

    public static void setTableMapperReverseMap(Map<String, String> tableMapperReverseMap) {
        FFTableMapperParam.tableMapperReverseMap = tableMapperReverseMap;
    }

    public static void clear() {
        tableMapperMap.clear();
    }

    public static String getOriginTableName(String exposeTableName) {
        if (tableMapperReverseMap.containsKey(exposeTableName)) {
            return tableMapperReverseMap.get(exposeTableName);
        }
        return exposeTableName;
    }
   /*
   @Param  原表名
   @return 映射表名
    */
    public static String getExposeTableName(String originTableName) {
        if (tableMapperMap.containsKey(originTableName)) {
            return tableMapperMap.get(originTableName);
        }
        return originTableName;
    }
}
