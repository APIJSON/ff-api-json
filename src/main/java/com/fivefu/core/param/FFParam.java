package com.fivefu.core.param;

import com.fivefu.core.exception.FfNoSupportException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author zhf
 * @Description
 * @Date 2021/2/18
 */
public class FFParam {

    public static final String KEY_DATASOURCE = "@ds"; //聚合函数条件，一般和@group一起用
    public static final String KEY_DATABASE = "@database"; //数据库类型，默认为MySQL
    public static final String KEY_CACHE = "@cache"; //缓存 RAM/ROM/ALL
    public static final String KEY_SCHEMA = "@schema"; //数据库，Table在非默认schema内时需要声明
    public static final String KEY_COLUMN = "@column"; //查询的Table字段或SQL函数
    public static final String KEY_FROM = "@from"; //FROM语句
    public static final String KEY_COMBINE = "@combine"; //条件组合，每个条件key前面可以放&,|,!逻辑关系  "id!{},&sex,!name&$"
    public static final String KEY_GROUP = "@group"; //分组方式
    public static final String KEY_HAVING = "@having"; //聚合函数条件，一般和@group一起用
    public static final String KEY_ORDER = "@order"; //排序函数
    public static final String KEY_ORDER_ASC = "@asc"; //升序
    public static final String KEY_ORDER_DESC = "@desc"; //降序，优先降序

    public static final String KEY_REOCRE_MAX_COUNT = "@maxRecordCount"; //聚合函数条件，一般和@group一起用

    //分页
    public static final String KEY_PAGE = "page"; //降序，优先降序
    public static final String KEY_PAGE_COUNT = "count"; //降序，优先降序
    public static final String KEY_IS_PAGE = "isPaging"; //降序，优先降序
    //where
    public static final String KEY_WHERE = "ffWhere";
    public static final String KEY_EQ = "eq";
    public static final String KEY_EQ_INT = "eqInt";
    public static final String KEY_NEQ = "!eq";
    public static final String KEY_NEQ_INT = "!eqInt";
    public static final String KEY_GT = "gt";//>
    public static final String KEY_GT_INT = "gtInt";//>
    public static final String KEY_GE = "ge";//>=
    public static final String KEY_GE_INT = "geInt";//>
    public static final String KEY_LT = "lt";//<
    public static final String KEY_LT_INT = "lt";//<
    public static final String KEY_LE = "le";//<=
    public static final String KEY_LE_INT = "leInt";//<=
    public static final String KEY_IN = "in";//in
    public static final String KEY_IN_INT = "inInt";//in
    public static final String KEY_NOT_IN_INT = "notInInt";//not in int类型
    public static final String KEY_NOT_IN = "notIn";//not in
    public static final String KEY_LiKE = "like";//KEY_NOT_IN

    public static Map<String, String> whereMap = new HashMap() {
        {
            put(KEY_EQ, " = ");
            put(KEY_EQ_INT, " = ");
            put(KEY_NEQ, " != ");
            put(KEY_NEQ_INT, " != ");
            put(KEY_GT, " > ");
            put(KEY_GT_INT, " > ");
            put(KEY_GE, " >= ");
            put(KEY_GE_INT, " >= ");
            put(KEY_LT, " < ");
            put(KEY_LT_INT, " < ");
            put(KEY_LE, " <= ");
            put(KEY_LE_INT, " <= ");
            put(KEY_IN, " in ");
            put(KEY_NOT_IN, " not in ");
            put(KEY_NOT_IN_INT, " not in ");
            put(KEY_IN_INT, " not in ");
            put(KEY_LiKE, " like ");
        }
    };

    //

    public static String dealWhereParam(String tableName, String column, String operator, String value)
            throws FfNoSupportException {
        operator = operator.trim();
        if (!FFParam.whereMap.containsKey(operator)) {
            throw new FfNoSupportException("暂不支持" + operator + " ");
        }

        if (!operator.endsWith("Int")) {
            value = "'" + value + "'";
        }
        switch (operator) {
            case FFParam.KEY_IN:
                value = "(" + value.replaceAll(",", "','") + ")";
                break;
            case FFParam.KEY_NOT_IN:
                value = "(" + value.replaceAll(",", "','") + ")";
                break;
            case FFParam.KEY_IN_INT:
                value = "(" + value + ")";
                break;
            case FFParam.KEY_NOT_IN_INT:
                value = "(" + value + ")";
                break;
            case FFParam.KEY_LiKE:
                ;
                return dealLike(tableName, column, value);
        }
        operator = FFParam.whereMap.get(operator);
        return "(" + tableName + "." + column + " " + operator + value + " )";
    }

    public static String dealWhereParamNoTableName(String column, String operator, String value)
            throws FfNoSupportException {
        operator = operator.trim();
        if (!FFParam.whereMap.containsKey(operator)) {
            throw new FfNoSupportException("暂不支持" + operator + " ");
        }

        if (!operator.endsWith("Int")) {
            value = "'" + value + "'";
        }
        switch (operator) {
            case FFParam.KEY_IN:
                value = "(" + value.replaceAll(",", "','") + ")";
                break;
            case FFParam.KEY_NOT_IN:
                value = "(" + value.replaceAll(",", "','") + ")";
                break;
            case FFParam.KEY_IN_INT:
                value = "(" + value + ")";
                break;
            case FFParam.KEY_NOT_IN_INT:
                value = "(" + value + ")";
                break;
            case FFParam.KEY_LiKE:
                return dealLike(column, value);
        }
        operator = FFParam.whereMap.get(operator);
        return "(" + column + " " + operator + value + " )";
    }

    public static String dealLike(String tableName, String column, String value) {

        return "";
    }

    public static String dealLike(String column, String value) {

        return  "(" + column + " like '% " + value + "%' )";
    }


}
