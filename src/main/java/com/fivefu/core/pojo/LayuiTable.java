package com.fivefu.core.pojo;

import java.io.Serializable;
import java.util.List;

public class LayuiTable<T>  implements Serializable {
   private Integer code;
   private String msg;
   private Integer count;
   private List<T> data;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
    public   static  LayuiTable renderError(String msg){
         LayuiTable layuiTable = new LayuiTable();
        layuiTable.setCode(500);
        layuiTable.setCount(0);
        layuiTable.setData(null);
        layuiTable.setMsg(msg);
        return  layuiTable;
    }
    public   static  LayuiTable renderSuccess(List data,int size){
        LayuiTable layuiTable = new LayuiTable();
        layuiTable.setCode(0);
        layuiTable.setCount(size);
        layuiTable.setData(data);
        layuiTable.setMsg("success");
        return  layuiTable;
    }
}
