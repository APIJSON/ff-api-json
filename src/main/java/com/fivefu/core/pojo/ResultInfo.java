package com.fivefu.core.pojo;

import java.io.Serializable;

public class ResultInfo implements Serializable{
	private static final long serialVersionUID = 9198443106600637875L;
	
	
	private Integer code;
	
	private String msg;
	
	private Object obj;
	
	
	private boolean flag;
	
	
	public ResultInfo(){
		
	}
	public ResultInfo(int code) {
		super();
		this.code = code;
	}

	public ResultInfo(Integer code,String msg){
		this.code=code;
		this.msg=msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	public ResultInfo setCodeBuild(Integer code){
		this.code = code;
		return this;
	}
	public ResultInfo setMsgBuild(String msg){
		this.msg = msg;
		return this;
	}
	public ResultInfo setObjBuild(Object obj){
		this.obj = obj;
		return this;
	}
	public ResultInfo setFlagBuild(boolean flag){
		this.flag = flag;
		return this;
	}

	public static ResultInfo renderSuccess(String msg){
		ResultInfo resultInfo  = new ResultInfo();
		resultInfo.setFlag(true);
		resultInfo.setCode(200);
		resultInfo.setMsg(msg);
		resultInfo.setObj(null);
		return resultInfo;
	}

	public  static ResultInfo renderSuccess(Object obj){
		ResultInfo resultInfo  = new ResultInfo();
		resultInfo.setFlag(true);
		resultInfo.setCode(200);
		resultInfo.setMsg("success");
		resultInfo.setObj(obj);
		return resultInfo;
	}

	public  static ResultInfo renderError(String msg){
		ResultInfo resultInfo  = new ResultInfo();
		resultInfo.setFlag(false);
		resultInfo.setCode(500);
		resultInfo.setMsg(msg);
		resultInfo.setObj(null);
		return resultInfo;
	}
	
}
