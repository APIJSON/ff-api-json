package com.fivefu.core.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/5
 */
public class FfSql {


    private static String OPERATE = "select ";
    private static String FROM = "From ";
    private String tablename ;
    private List<String>  unProcessedTableNameList = new ArrayList<>(3);
    private String columnName;
    private String where = "";
    private String group = "";
    private String having = "";
    private String order = "";
    private String limit = "";
    private int maxCount = 1000;
    private String commonSql;
    private String countSql;

    private String ds = "master";

    public String getDs() {
        return ds;
    }

    public void setDs(String ds) {
        this.ds = ds;
    }

    private Boolean pageFlag = false;

    public Boolean getPageFlag() {
        return pageFlag;
    }

    public void setPageFlag(Boolean pageFlag) {
        this.pageFlag = pageFlag;
    }

    public static String getOPERATE() {
        return OPERATE;
    }


    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public static String getFROM() {
        return FROM;
    }



    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename;
    }

    public List<String> getUnProcessedTableNameList() {
        return unProcessedTableNameList;
    }

    public void setUnProcessedTableNameList(List<String> unProcessedTableNameList) {
        this.unProcessedTableNameList = unProcessedTableNameList;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getHaving() {
        return having;
    }

    public void setHaving(String having) {
        this.having = having;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String createCommonSql() {
         this.commonSql =  "select * from ( "+OPERATE + " "+ this.columnName+" "+FROM +" " + tablename
                     + " "+ where +" " + group +" "+ having +" " + order+" "+ limit +" ) _tem limit "+ maxCount;
         return  this.commonSql;
    }

    public String createCountSql() {
        this.countSql =  "select count(*) as count from ( "+OPERATE + " "+ this.columnName+" "+FROM +" " + tablename
                + " "+ where +" " + group +" "+ having +" " + order +" ) _tem limit "+ maxCount;
        return  this.countSql;
    }
}
