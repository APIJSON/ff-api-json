package com.fivefu.core.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/8
 */
@TableName("table_mapper")
public class FFTableMapper {

    private Integer id;
    @TableField("expose_table_name")
    private String exposeTableName;
    @TableField("origin_table_name")
    private String originTableName;
    @TableField("is_valid")
    private Boolean isValid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExposeTableName() {
        return exposeTableName;
    }

    public void setExposeTableName(String exposeTableName) {
        this.exposeTableName = exposeTableName;
    }

    public String getOriginTableName() {
        return originTableName;
    }

    public void setOriginTableName(String originTableName) {
        this.originTableName = originTableName;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }
}
