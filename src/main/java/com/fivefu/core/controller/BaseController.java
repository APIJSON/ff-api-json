package com.fivefu.core.controller;

import com.fivefu.core.pojo.ResultInfo;
import com.fivefu.core.ffEnum.ErrorEnum;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
public class BaseController {

    protected ResultInfo getErrMsg(ErrorEnum errorEnum){
         return  new ResultInfo(errorEnum.getCode(),errorEnum.getMsg());
    }

}
