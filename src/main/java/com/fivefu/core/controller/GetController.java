package com.fivefu.core.controller;

import com.alibaba.fastjson.JSONObject;
import com.fivefu.core.context.DynamicDataSourceContextHolder;
import com.fivefu.core.handler.SqlHandlerChain;
import com.fivefu.core.pojo.ResultInfo;
import com.fivefu.core.exception.FFNoSuchTableException;
import com.fivefu.core.exception.FfSqlAnalysisException;
import com.fivefu.core.parse.FfExecuteSql;
import com.fivefu.core.pojo.FfSql;
import com.fivefu.core.handler.ParseRequest;
import com.fivefu.core.param.FFParam;
import com.fivefu.core.service.FfCacheService;
import com.fivefu.core.service.FfDataSourceService;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
@RestController
@RequestMapping("/get")
public class GetController extends BaseController {

    @Autowired
    FfDataSourceService ffDataSourceService;
    @Autowired
    FfExecuteSql ffExecuteSql;
    @Autowired
    FfCacheService cacheService;
    @Autowired
    SqlHandlerChain sqlHandlerChain;

    @RequestMapping("/get")
    public ResultInfo test(@RequestParam("key") String key) {

        DynamicDataSourceContextHolder.setContextKey(key);
        //查询表信息
        List<Map<String, Object>> tables = ffDataSourceService.getTableInfo();
        DynamicDataSourceContextHolder.removeKey();
        return ResultInfo.renderSuccess(tables);


    }

    @RequestMapping("/execute")
    public ResultInfo test(HttpServletRequest request)
            throws FFNoSuchTableException, FfSqlAnalysisException, IOException {

        JSONObject json = ParseRequest.parse(request);
        FfSql ffSql = new FfSql();

        //链式处理
        sqlHandlerChain.handler(json, ffSql);
        //缓存处理
        if (json.containsKey(FFParam.KEY_CACHE)) {
            return ResultInfo.renderSuccess(cacheService.getResultByKey(json, ffSql));
        }

        Object result = ffExecuteSql.executeFFSql(ffSql);
        return ResultInfo.renderSuccess(result);


    }


    public ResultInfo convertSsqlToJsonParam(String sql) {
        String sqlTMp = sql.trim().toLowerCase();//去掉空格 并且全部小写·
        //第一步 去掉前后空格
        if (sql.startsWith("select")) {
            sqlTMp = sqlTMp.substring(sql.indexOf("select") + "select".length()).trim();
            String cloumns[] = sqlTMp.substring(0, sqlTMp.indexOf("from")).split(",");

            sqlTMp = sqlTMp.substring(sqlTMp.indexOf("from") + "from".length());
            String tableNameWithJoin = "";
            if (sqlTMp.contains("where")) {
                tableNameWithJoin = sqlTMp.substring(0, sqlTMp.indexOf("where"));
            }

        } else {
            return ResultInfo.renderError("语句必须以select开始");
        }
        return  null;
    }

}
