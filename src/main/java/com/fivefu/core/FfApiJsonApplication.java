package com.fivefu.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @Author zhf
 * @Description
 * @Date 2021/3/4
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class FfApiJsonApplication {

    public static void main(String[] args) {

        SpringApplication.run(FfApiJsonApplication.class, args);
    }



}
